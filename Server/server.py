from flask import Flask, request
import os

app = Flask(__name__)

# 设置保存图片的目录
UPLOAD_FOLDER = 'uploads'
if not os.path.exists(UPLOAD_FOLDER):
    os.makedirs(UPLOAD_FOLDER)

@app.route('/upload', methods=['POST'])
def upload_image():
    try:
        file_data = request.data
        if not file_data:
            return "No data received", 400
        
        # 给文件命名，可以使用时间戳或者其他方式生成唯一的文件名
        file_path = os.path.join(UPLOAD_FOLDER, 'uploaded_image.jpg')
        
        # 保存图片
        with open(file_path, 'wb') as f:
            f.write(file_data)
        
        return f"File uploaded successfully: {file_path}", 200
    
    except Exception as e:
        return str(e), 500

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1080)
